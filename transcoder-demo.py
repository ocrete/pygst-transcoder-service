#!/usr/bin/env python3

# First pip3 install aiohttp & gbulb

import sys
import os

from aiohttp import web
import gbulb

import gi.repository
gi.require_version('Gst', '1.0')
from gi.repository import Gst


class TranscodeOutputCreationError(Exception):
    pass


class TranscodeOutput:

    def __init__(self, id):
        self.id = id

    def link_stream(self, p, pad):
        raise NotImplemented()

    def set_port(self, port):
        raise NotImplemented()

    def get_info(self):
        raise NotImplemented()

    @staticmethod
    def get_routes():
        return []


class TranscodeOutputHls(TranscodeOutput):
    instances = []

    def __init__(self, id, params):
        TranscodeOutput.__init__(self, id)

        self.__muxer = Gst.ElementFactory.make("hlssink2")
        self.added = False
        TranscodeOutputHls.instances.append(self)

    def __del__(self):
        TranscodeOutputHls.instances.remove(self)

    def __add_muxer_to_pipeline(self, p):
        if self.added:
            return
        p.add(self.__muxer)
        self.__muxer.set_state(Gst.State.PLAYING)
        self.added = True

    def __remove_muxer_from_pipeline(self, p):
        p.remove(self.__muxer)
        self.__muxer.set_state(Gst.State.NULL)

    def link_stream(self, p, pad):
        caps = pad.query_caps()  # Why is pad.get_current_caps() not working ?
        if caps.get_structure(0).has_name("video/x-raw"):
            enc = Gst.ElementFactory.make("x264enc")
            parser = Gst.ElementFactory.make("h264parse")
        elif caps.get_structure(0).has_name("audio/x-raw"):
            enc = Gst.ElementFactory.make("avenc_aac")
            parser = Gst.ElementFactory.make("aacparse")
        p.add(enc)
        p.add(parser)
        pad.link(enc.get_static_pad("sink"))
        enc.link(parser)
        enc.set_state(Gst.State.PLAYING)
        parser.set_state(Gst.State.PLAYING)

        self.__add_muxer_to_pipeline(p)

        parser.link(self.__muxer)

    def set_port(self, port):
        self.in_port = port
        self.hls_path = "/%d/%s/playlist.m3u8" % (port, self.id)

    @staticmethod
    def get_routes():
        return [web.get("/{port}/{id}/{file}",
                        TranscodeOutputHls.__get_output)]

    @staticmethod
    def __get_output(request):
        if 'port' not in request.match_info or \
           'id' not in request.match_info or \
           'file' not in request.match_info:
            raise web.HTTPNotFound()
        port = int(request.match_info['port'])
        id = request.match_info['id']
        filename = request.match_info['file']
        for i in TranscodeOutputHls.instances:
            if i.in_port == port and i.id == id:
                return web.FileResponse(filename)
        raise web.HTTPNotFound()

    def get_info(self):
        return {'id': self.id,
                'type': 'hls',
                'hlspath': self.hls_path}


class TranscodeOutputTsUdp(TranscodeOutput):

    def __init__(self, id, params):
        TranscodeOutput.__init__(self, id)

        if 'port' not in params:
            raise TranscodeOutputCreationError("TS-UDP output requires port")
        if 'host' not in params:
            raise TranscodeOutputCreationError("TS-UDP output requires host")
        self.out_port = int(params['port'])
        if self.out_port < 1 or self.out_port > 65535:
            raise TranscodeOutputCreationError("TS-UDP output port invalid")

        self.out_host = str(params['host'])
        self.__sink = Gst.parse_bin_from_description(
            "mpegtsmux alignment=7 ! udpsink port=%d host=\"%s\" " %
            (self.out_port, self.out_host), True)
        self.added = False

    def __add_muxer_to_pipeline(self, p):
        if self.added:
            return
        p.add(self.__sink)
        self.__muxer.set_state(Gst.State.PLAYING)
        self.added = True

    def __remove_muxer_from_pipeline(self, p):
        p.remove(self.__sink)
        self.__sink.set_state(Gst.State.NULL)

    def link_stream(self, p, pad):
        caps = pad.query_caps()  # Why is pad.get_current_caps() not working ?
        if caps.get_structure(0).has_name("video/x-raw"):
            enc = Gst.ElementFactory.make("x264enc")
            parser = Gst.ElementFactory.make("h264parse")
        elif caps.get_structure(0).has_name("audio/x-raw"):
            enc = Gst.ElementFactory.make("avenc_aac")
            parser = Gst.ElementFactory.make("aacparse")
        p.add(enc)
        p.add(parser)
        pad.link(enc.get_static_pad("sink"))
        enc.link(parser)
        enc.set_state(Gst.State.PLAYING)
        parser.set_state(Gst.State.PLAYING)

        self.__add_muxer_to_pipeline(p)

        parser.link(self.__sink)

    def set_port(self, port):
        self.in_port = port
        self.hls_path = "/%d/%s/playlist.m3u8" % (port, self.id)

    def get_info(self):
        return {'id': self.id,
                'type': 'tsudp',
                'port': self.out_port,
                'host': self.out_host}


class TranscodePipeline:

    def __init__(self, port):
        # stored params
        self.port = port

        # internal state
        self.error = None
        self.streams = []

        self.outputs = {}

        # the pipeline
        self.p = Gst.Pipeline()
        self.src = Gst.ElementFactory.make("uridecodebin3")
        self.src.props.uri = "udp://0.0.0.0:" + str(port)
        self.src.connect("pad-added", self.__src_pad_added)
        self.p.add(self.src)

        # connect the bus
        bus = self.p.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message::error", self.__bus_error)
        bus.connect("sync-message::stream-collection",
                    self.__bus_stream_collection)

    def __run(self):
        ret = self.p.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            self.p.set_state(Gst.State.NULL)
            return False
        return True

    def __bus_error(self, bus, msg):
        (gerr, dbg) = msg.parse_error()
        print(gerr.message)
        print(dbg)
        self.error = dbg

    def __bus_stream_collection(self, bus, msg):
        c = msg.parse_stream_collection()
        selected_streams = []
        for i in range(0, c.get_size()):
            s = c.get_stream(i)
            if s.get_stream_type() in (Gst.StreamType.AUDIO,
                                       Gst.StreamType.VIDEO):
                selected_streams += [s.get_stream_id()]
        self.src.send_event(Gst.Event.new_select_streams(selected_streams))

    def __src_pad_added(self, element, pad):
        for o in self.outputs:
            self.outputs[o].link_stream(self.p, pad)

    def add_output(self, output):
        self.outputs[output.id] = output
        output.set_port(self.port)
        for pad in self.src.iterate_src_pads():
            output.link_stream(self.p, pad)
        return self.__run()


class TranscodeServer:

    def __init__(self):
        self.app = web.Application()
        self.app.add_routes([web.put('/{port}', self.create)])
        self.app.add_routes([web.get('/{port}', self.get)])
        self.app.add_routes(TranscodeOutputHls.get_routes())
        self.pipelines = {}

    def run(self):
        web.run_app(self.app)

    """Example:
    curl --header "Content-Type: application/json" --request PUT --data '{"outputs": [{"type":"hls", "id": "myhls"}, {"type": "tsudp", "id": "myudp", "params": { "port":5002}} ]}' http://localhost:8080/5000
    """
    async def create(self, request):
        json = await request.json()

        port = int(request.match_info['port'])
        if port in self.pipelines:
            raise web.HTTPConflict(text="Pipeline already exists")

        p = TranscodePipeline(port=port)

        if 'outputs' in json and json['outputs']:
            for o in json['outputs']:
                if 'type' not in o:
                    raise web.HTTPBadRequest(text='Output is missing type')
                if 'id' not in o:
                    raise web.HTTPBadRequest(text='Output is missing id')
                id = str(o['id'])
                params = o['params'] if 'params' in o and \
                    isinstance(o['params'], dict) else {}
                output = None
                try:
                    if o['type'] == 'hls':
                        output = TranscodeOutputHls(id, params)
                    elif o['type'] == 'tsudp':
                        output = TranscodeOutputTsUdp(id, params)
                except TranscodeOutputCreationError as e:
                    raise web.HTTPBadRequest(text=str(e))
                if output and not p.add_output(output):
                    raise web.HTTPBadRequest(text='Could not start pipeline')
        self.pipelines[port] = p

        return await self.get(request)

    """
    Example:
    curl --header "Content-Type: application/json" --request GET http://localhost:8080/5000
    """
    async def get(self, request):
        port = int(request.match_info['port'])
        if port not in self.pipelines:
            raise web.HTTPNotFound()

        out_list = []
        for o in self.pipelines[port].outputs:
            out_list += [self.pipelines[port].outputs[o].get_info()]

        return web.json_response({"port": port, "outputs": out_list})


if __name__ == '__main__':
    Gst.init(sys.argv)
    gbulb.install()
    server = TranscodeServer()
    server.run()
