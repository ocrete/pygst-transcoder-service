## Example GStreamer based transcoder web service ##

This is a simple example of simple Python RESTful web service that
implements the most simple live transcoder.

Features are very limited:

 * Takes in anything that can be streamed over UDP, has been tested with MPEG-TS over UDP and MPEG-TS over RTP over UDP
 * Can produce 2 outputs:
   * H.264 & AAC HLS
   * H.264 & AAC in MPEG-TS over UDP

It requires:
 * Python 3
 * GStreamer 1.x
 * Python-GStreamer 1.x
 * From pip:
   * gbulb
   * aiohttp

Run it on the command line and it can then be controlled using a REST API, to create a transcode, do a put using the source port as the ID, the following example will create a source that listens on port 5000, transcodes to HLS and sends the output also as a MPEG-TS over UDP on port 5002 on localhost.

`curl --header "Content-Type: application/json" --request PUT --data '{"outputs": [{"type":"hls", "id": "myhls"}, {"type": "tsudp", "id": "myudp", "params": { "port":5002, "host": "localhost"}} ]}' http://localhost:8080/5000`

